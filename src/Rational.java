/**
 * Класс, совершающий дейсттвия с дробями
 */
class Rational {
    private int num;
    private int denum;

    /**
     * Конструктор с параметрами
     * @param num числитель дроби
     * @param denum знаменатель дроби
     */
    public Rational(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    /**
     * Конструктор по умолчанию
     */
    public Rational() {
        this(1, 1);
    }

    /**
     * конструктор с 1 параметром
     * @param num числитель дроби
     */
    public Rational(int num) {
        this(num, 1);
    }

    /**
     * Метод деления дроби на числитель и знаменатель
     * @param stringRacional дробь
     */
    public void racionalSplit(String stringRacional) {
        String[] array = new String[3];
        int i = 0;

        for (String splitstring : stringRacional.split("/")) {
            array[i] = splitstring;
            i++;
        }

        this.num = Integer.parseInt(array[0]);
        this.denum = Integer.parseInt(array[1]);
    }

    /**
     *  *ВРЕМЕННО НЕ РАБОТАЕТ*
     * Метод сокращения дроби
     * @return сокращенная дробь
     */
    public Rational reduction() {
        Rational racional = new Rational();
        int nod;
        try {
            while (this.num != 0 && this.denum != 0) {
                if (this.num > this.denum) {
                    nod = this.num %= this.denum;
                } else {
                    nod = this.denum %= this.num;
                }
            }
        } catch (ArithmeticException e) {
            throw new ArithmeticException("Деление на 0");
        }
        return racional;
    }

    /**
     * Метод вывода на экран дроби вида x/y
     * @return строка вида x/y
     */
    @Override
    public String toString() {
        return num + "/" + denum;
    }

    /**
     *  Метод, совершающий сложение дробей
     * @param racional2 дробь вида x/y
     * @return результат сложения дробей
     */
    public Rational summ(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum + this.denum * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    /**
     * Метод, совершающий вычитание дробей
     * @param racional2  дробь вида x/y
     * @return результат
     */
    public Rational subtraction(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum - this.denum * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    /**
     * Метод, совершающий умножение дробей
     * @param racional2  дробь вида x/y
     * @return результат умножения дробей
     */
    public Rational multiplication(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    /**
     *  Метод, совершающий деление дробей
     * @param racional2  дробь вида x/y
     * @return результат деления дробей
     */
    public Rational division(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum;
        racional.denum = this.denum * racional2.num;
        return racional;
    }

}
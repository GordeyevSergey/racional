
import java.io.*;
/**
 *
 */
class Main {
    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        File output = new File("src\\output.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output));
        Rational rational1 = new Rational();
        Rational rational2 = new Rational();
        String stringRational;
        String[] array = new String[100];


        while ((stringRational = bufferedReader.readLine()) != null) {
            int i = 0;
            for (String minstring : stringRational.split(" ")) {
                array[i] = minstring;
                i++;
            }

            try {
                rational1.racionalSplit(array[0]);
                rational2.racionalSplit(array[2]);
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Не соблюдается формат строки");
            }

            switch (array[1]) {
                case "+":
                    System.out.println(stringRational + " = " + rational1.summ(rational2));
                    bufferedWriter.write(stringRational + " = " + rational1.summ(rational2) + "\n");
                    break;
                case "-":
                    System.out.println(stringRational + " = " + rational1.subtraction(rational2));
                    bufferedWriter.write(stringRational + " = " + rational1.subtraction(rational2) + "\n");
                    break;
                case "*":
                    System.out.println(stringRational + " = " + rational1.multiplication(rational2));
                    bufferedWriter.write(stringRational + " = " + rational1.multiplication(rational2) + "\n");
                    break;
                case "/":
                    System.out.println(stringRational + " = " + rational1.division(rational2));
                    bufferedWriter.write(stringRational + " = " + rational1.division(rational2) + "\n");
                    break;
                default:
                    System.out.println("Неправильно введены данные");
                    break;
            }
        }
        bufferedReader.close();
        bufferedWriter.close();
    }
}